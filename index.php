<?php
	session_start();

    $cookie_name = "cm";
    $cookie_val = "many";
    setcookie($cookie_name, $cookie_val, time() + 86400 * 30, "/");

?>
<!DOCTYPE html>
<html>
<head>
	<title>Thryty</title>
	<link rel="stylesheet" type="text/css" href="main.css" />
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<!-- ft awesome icons -->
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</head>
<body>
	<div id="nav-thryty" class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header"><a class="navbar-brand" href="#">Thryty</a>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-menubuilder"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse navbar-menubuilder">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Home</a>
                </li>
                <li><a href="#">#You</a>
                </li>
                <li><a href="#">Maps</a>
                </li>
                <li><a href="#">Music</a>
                </li>
                <li><a href="#">Images</a>
                </li>
                <li><a href="#">Videos</a>
                </li>
            </ul>
        </div>
    </div>
</div>
<br>
<br>
<br>
<br>
<br>
<br>
<div class="doodle" align="center">
	<a href="#" title="Happy CNY from our team!">Thryty</a>
</div>
<div class="box">
  <div class="container-4">
  <form action="search.php" method="POST">
  	<input type="text" name="search" id="search" placeholder="Search..." />
    <input type="submit" name="sumbit" value=""><button class="icon"><i class="fa fa-search"></i></button>
  </form>
  </div>
</div>
<div class="footer">
	<p>&copy; 2015 - <?php $year = date('Y'); echo "$year"; ?> Thryty, Inc.</p>
	<ul id="links">
		<li><a href="contactus.php">Contact Us</a></li>
		<li><a href="career.php">Career</a></li>
		<li><a href="internship.php">Internship</a></li>
	</ul>
</div>
</body>
</html>