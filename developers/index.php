﻿<?php
	session_start();

	/**variables
	$cookie_name = "ckie_thrtbx";
	$cookie_value ="";

	setcookie($cookie_name, $cookie_value, time() + 84600 * 60);
	*/
?>

<!DOCTYPE html>
<html>
<head>
	<title>Thrytbox - Developers</title>

	<link rel="stylesheet" type="text/css" href="main.css" />
	<style type="text/css">
		#cssmenu {background: #333;border-bottom: 4px solid #1b9bff;font-family: 'Oxygen Mono', Tahoma, Arial, sans-serif;font-size: 12px; z-index: 9;}
		#cssmenu a {
background: #333;
color: #CBCBCB;
padding: 0 20px;
}
#cssmenu ul { text-transform: uppercase; }

#cssmenu ul ul {
border-top: 4px solid #1b9bff;
text-transform: none;
min-width: 190px;
}
#cssmenu ul ul a {
background: #1b9bff;
color: #FFF;
border: 1px solid #0082e7;
border-top: 0 none;
line-height: 150%;
padding: 16px 20px;
}
#cssmenu ul ul ul { border-top: 0 none; }

#cssmenu ul ul li { position: relative }

#cssmenu > ul { *display: inline-block; }

#cssmenu:after, #cssmenu ul:after {
content: '';
display: block;
clear: both;
}
#cssmenu > ul > li > a { line-height: 48px; }

#cssmenu ul ul li:first-child > a { border-top: 1px solid #0082e7; }
#cssmenu ul ul li:hover > a { background: #35a6ff; }

#cssmenu ul ul li:last-child > a {
border-radius: 0 0 3px 3px;
box-shadow: 0 1px 0 #1b9bff;
}
#cssmenu ul ul li:last-child:hover > a { border-radius: 0 0 0 3px; }

#cssmenu ul ul li.has-sub > a:after {
content: '+';
position: absolute;
top: 50%;
right: 15px;
margin-top: -8px;
}
#cssmenu ul li:hover > a, #cssmenu ul li.active > a {
background: #1b9bff;
color: #FFF;
}
#cssmenu ul li.has-sub > a:after {
content: '+';
margin-left: 5px;
}
#cssmenu ul li.last ul {
left: auto;
right: 0;
}
#cssmenu ul li.last ul ul {
left: auto;
right: 99.5%;
}

/* code for animated blinking cursor */
        .typed-cursor{
            opacity: 1;
            font-weight: 100;
            -webkit-animation: blink 0.7s infinite;
            -moz-animation: blink 0.7s infinite;
            -ms-animation: blink 0.7s infinite;
            -o-animation: blink 0.7s infinite;
            animation: blink 0.7s infinite;
        }
        @-keyframes blink{
            0% { opacity:1; }
            50% { opacity:0; }
            100% { opacity:1; }
        }
        @-webkit-keyframes blink{
            0% { opacity:1; }
            50% { opacity:0; }
            100% { opacity:1; }
        }
        @-moz-keyframes blink{
            0% { opacity:1; }
            50% { opacity:0; }
            100% { opacity:1; }
        }
        @-ms-keyframes blink{
            0% { opacity:1; }
            50% { opacity:0; }
            100% { opacity:1; }
        }
        @-o-keyframes blink{
            0% { opacity:1; }
            50% { opacity:0; }
            100% { opacity:1; }
        }
	</style>
	<!-- Get jQuery -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="js/typed.js" type="text/javascript"></script>
    <script>
    $(function(){

        $("#typed").typed({
            // strings: ["Typed.js is a <strong>jQuery</strong> plugin.", "It <em>types</em> out sentences.", "And then deletes them.", "Try it out!"],
            stringsElement: $('#typed-strings'),
            typeSpeed: 30,
            backDelay: 500,
            loop: false,
            contentType: 'html', // or text
            // defaults to false for infinite loop
            loopCount: false,
            callback: function(){ foo(); },
            resetCallback: function() { newTyped(); }
        });

        $(".reset").click(function(){
            $("#typed").typed('reset');
        });

    });

    function newTyped(){ /* A new typed object */ }

    function foo(){ console.log("Callback"); }

    $(window, document, undefined).ready(function() {

  $('input').blur(function() {
    var $this = $(this);
    if ($this.val())
      $this.addClass('used');
    else
      $this.removeClass('used');
  });

  var $ripples = $('.ripples');

  $ripples.on('click.Ripples', function(e) {

    var $this = $(this);
    var $offset = $this.parent().offset();
    var $circle = $this.find('.ripplesCircle');

    var x = e.pageX - $offset.left;
    var y = e.pageY - $offset.top;

    $circle.css({
      top: y + 'px',
      left: x + 'px'
    });

    $this.addClass('is-active');

  });

  $ripples.on('animationend webkitAnimationEnd mozAnimationEnd oanimationend MSAnimationEnd', function(e) {
  	$(this).removeClass('is-active');
  });

});
    </script>
</head>
<body>
<div class="background-wrap">
			<video id="video-bg-elem" preload="auto" autoplay="true" loop="loop" muted="muted"> 
				<source src="vidbg.mp4" type="video/mp4">
				Video not supported
			</video>          
		</div>
		<div class="content">
		<h1>thrytbox for developers</h1>
		<div class="wrap" align="center" style="display: block; font-family: sans-serif; color: white;">
        <div class="type-wrap">
            <div id="typed-strings">
                <span>We're built<strong> by</strong> students.</span>
                <p>Made <em>for </em> people</p>
                <p>And coded with love</p>
                <p>Try it out!</p>
            </div>
            <span id="typed" style="white-space: pre-line;"></span>
        </div>
</div>
<h2 style="text-align: center; font-family: sans-serif; color: white;">Login</h2>
		<form action="login.php">
  			<div class="group">
    		<input type="text"><span class="highlight"></span><span class="bar"></span>
    		<label>Username</label>
  			</div>
  			<div class="group">
    		<input type="password"><span class="highlight"></span><span class="bar"></span>
    		<label>Password</label>
  			</div>
  <button type="button" class="button buttonBlue" />Login
    <div class="ripples buttonRipples"><span class="ripplesCircle"></span></div>
  </button>
  <h2 style="font-family: sans-serif; font-size: 1em;">No account? <a href="#" style="text-decoration: none;">Sign up!</a></h2>
</form>
</div>
</body>
</html>